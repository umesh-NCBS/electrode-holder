use <threadlib/threadlib.scad>

$fn=100;

module body() {
  difference() {
    cylinder(d=10, h=7.5);
    cylinder(d=4, h=7.5);
  }

  translate([0,0,7.5]) {
    difference() {
      intersection() {
        union() {
          cylinder(d=6, h=8);
          translate([0,0,8]) {
            cylinder(d1=6, d2=2.4, h=7);
          }
        }
        bolt("M6", turns=15);
      }
    cylinder(d=1.25, h=15);
    translate([-3,-0.2,2]) {
      cube([6,0.4,13]);
    }
    }
  }
}

module cap() {
  intersection() {
    nut("M6", turns=10, Douter=10);
    cylinder(d=10, h=10);
  }
  translate([0,0,10]) {
    difference() {
      cylinder(d1=10, d2=6, h=3);
      cylinder(d=3.5, h=3);
    }
  }
}

body();
translate([11,0,0]) {
  cap();
}